<?php

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>"; 
echo "cold_blooded : " . $sheep->cold_blooded . "<br><br>"; 

require_once('Ape.php');

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold_blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->Yell() . "<br><br>";

require_once('Frog.php');

$kodok = new Frog("buduk");

echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold_blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->Jump();


?>