<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Form()
    {
        return view ('page.Form');
    }

    public function Home(Request $request)
    {
        $NamaDepan = $request ['fname']; 
        $NamaBelakang = $request ['lname']; 

        return view ('page.Home', ['NamaDepan'=>$NamaDepan, 'NamaBelakang'=>$NamaBelakang]);
    }
}
