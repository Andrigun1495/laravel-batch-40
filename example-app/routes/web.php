<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[IndexController::class, 'Index']);
Route::get('/Form', [AuthController::class, 'Form']);
Route::post('/Home', [AuthController::class, 'Home']);

Route::get('/data-tables', [IndexController::class, 'Table']);

//CRUD

//Create Data
//Route untuk mengarah ke form tambah Cast
Route::get('/cast/create', [CastController::class,'create']);
 // Simpan data Cast di database
 Route::post('/cast', [CastController::class,'store']);

 // Read Data
 // Tampil semua data di table
 Route::get('/cast', [CastController::class, 'index']);
 // Detail cast berdasarkan id nya
 Route::get('/cast/{cast_id}', [CastController::class, 'show']);
 // Update Data
 //route untuk mengarah ke form edit cast berdasarkan id
 Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
 //update data berdasarkan id
 Route::put('/cast/{cast_id}', [CastController::class, 'update']);
 //Delete data
 Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

//Create Data Genre
//Route untuk mengarah ke form tambah Genre
Route::get('/genre/create', [GenreController::class,'create']);
 // Simpan data genre  di database
 Route::post('/genre', [GenreController::class,'store']);

 // Read Data
 // Tampil semua data di table
 Route::get('/genre', [GenreController::class, 'index']);
 // Detail genre berdasarkan id nya
 Route::get('/genre/{genre_id}', [GenreController::class, 'show']);
 // Update Data
 //route untuk mengarah ke form edit genre  berdasarkan id
 Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
 //update data berdasarkan id
 Route::put('/genre/{genre_id', [GenreController::class, 'update']);
 //Delete data
 Route::delete('/genre/{genre_id', [GenreController::class, 'destroy']);

