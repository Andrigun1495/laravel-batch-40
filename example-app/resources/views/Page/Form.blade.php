@extends('layout.master')
@section('title')
    Halaman Registrasi
@endsection
@section('subtitle')
    Register
@endsection
@section('content')
<h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <form action="/Home" method="post">
    @csrf
        <label>First Name:</label><br>
        <input type="text" name="fname"> <br><br>
        <label>Last Name:</label><br>
            <input type="text" name="lname"> <br><br>
            <label>Gender:</label><br>
				<?php
				$jenis_kelamin = array('L' => 'Laki Laki', 'P' => 'Perempuan');
				foreach ($jenis_kelamin as $kode => $detail) {
					$checked = @$_POST['jenis_kelamin'] == $kode ? ' checked="checked"' : '';
					echo '<label class="radio">
							<input name="jenis_kelamin" type="radio" value="' . $kode . '"' . $checked . '>' . $detail . '</option>
						</label>';
				}
				?><br><br>
        <label>Nationally</label><br>
        <select name="Indonesia"> <br>
            <option value="">Indonesia</option>
            <option value="">English</option>
            <option value="">Japan</option>
        </select> <br> <br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br><br>
        <label>Bio</label><br>
        <textarea name="Bio" cols="50"rows="10"></textarea><br>
        <input type="submit" value="Sign Up"><br><br>
    </form>
@endsection