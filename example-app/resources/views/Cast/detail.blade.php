@extends('layout.master')
@section('title')
    Halaman Detail Cast
@endsection
@section('subtitle')
    Data Aktor
@endsection
@section('content')

<h2>{{$cast->nama}}</h2>
<h3>{{$cast->umur}}</h3>
<p> {{$cast->bio}}</p>

@endsection