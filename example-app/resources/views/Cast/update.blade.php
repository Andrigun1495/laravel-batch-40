@extends('layout.master')
@section('title')
    Halaman Update Cast
@endsection
@section('subtitle')
    Cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label >Nama Cast</label>
      <input type="text" class="form-control" value="{{$cast->nama}}"  name="nama">
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="form-group">
        <label >Umur</label>
        <input type="text" class="form-control" value="{{$cast->umur}}"name="umur">
        @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      </div>
    <div class="form-group">
      <label >Bio</label>
     <textarea name="bio" class="form-control" cols="30" rows="10">value="{{$cast->bio}}"</textarea>
     @error('bio')
     <div class="alert alert-danger">{{ $message }}</div>
 @enderror
    </div>
  
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    </form>
@endsection