@extends('layout.master')
@section('title')
    Halaman Update Genre
@endsection
@section('subtitle')
    Genre
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label >Nama Genre</label>
      <input type="text" class="form-control" value="{{$cast->nama}}"  name="nama">
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
 
  
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    </form>
@endsection