@extends('layout.master')
@section('title')
    Halaman List Film
@endsection
@section('subtitle')
    List Film
@endsection
@section('content')

<form action="/film" method="POST" enctype="multipart/form-data">
  @csrf
    <div class="form-group">
      <label >Judul Film</label>
      <input type="text" class="form-control" name="judul">
      @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="form-group">
        <label >Ringkasan</label>
       <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
       @error('content')
       <div class="alert alert-danger">{{ $message }}</div>
   @enderror
<div class="form-group">
    <label >Tahun</label>
    <input type="text" class="form-control" name="tahun">
    @error('tahun')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
      </div>
      <div class="form-group">
        <label >Poster</label>
        <input type="file" name="Poster" class="form-control">
        @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <div class="form-group">
        <label >genre</label>
        <select name="genre_id" class="form-control"id="">
            @forelse ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @empty
                <option value="">Tidak Ada Data Genre</option>
            @endforelse
        </select>
        @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <button type="file" class="btn btn-primary">Submit</button>
  </form>
    </form>
@endsection